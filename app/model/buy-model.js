import mongoose, { Schema } from "mongoose";
import moment from "moment-timezone";

let buy = new Schema({
  transactionId: String,
  currency: String,
  units: Number,
  total: Number,
  sold: {
    type: Boolean,
    default: false,
  },
  unitPrice: Number,
  fees: Number,
  trailingStop: {
    type: Number,
    default: 0,
  },
  targetPrice: {
    type: Number,
    default: 0,
  },
  stopLossPrice: {
    type: Number,
    default: 0,
  },
  date: {
    type: Number,
    default: moment(),
  },
  sell: {
    type: Schema.Types.ObjectId,
    ref: "Sell",
  },
}, {
  toJSON: {
    virtuals: true,
  },
  toObject: {
    virtuals: true,
  }
});

buy.virtual("totalPricePerUnit").get(function() {
  return (this.total + this.fees) / this.units;
});

let Buy = mongoose.model("Buy", buy);

module.exports = Buy;
