import mongoose, { Schema } from "mongoose";
import moment from "moment-timezone";

let sell = new Schema({
  transactionId: String,
  currency: String,
  units: Number,
  total: Number,
  fees: Number,
  date: {
    type: Number,
    default: moment(),
  },
});

let Sell = mongoose.model("Sell", sell);

module.exports = Sell;
