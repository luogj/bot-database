import Mongoose from "mongoose";

import Buy from "./model/buy-model";
import Sell from "./model/sell-model";

let mongoose;
function init(mongoDbUri) {
  return new Promise((resolve) => {
    mongoose = Mongoose.connect(mongoDbUri);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on("error", (err) => {
      console.error("Mongoose encountered an error!");
      console.error(err);
      console.error("");
    });
    db.on("open", () => {
      console.log("Connected to MongoDB!");
      console.log("");
      resolve();
    });
  });
}

function saveBuyToDb(transaction) {
  return new Promise((resolve) => {
    Buy.create({
      transactionId: transaction.id,
      currency: transaction.currency,
      units: transaction.units,
      total: transaction.total,
      unitPrice: transaction.unitPrice,
      fees: transaction.fees,
    }).then((buy) => {
      console.log("Buy of Transaction ID " + buy.transactionId + " saved to database!");
      console.log("");
      resolve();
    });
  });
}

function saveSellToDb(transaction) {
  return new Promise((resolve) => {
    Sell.create({
      transactionId: transaction.id,
      currency: transaction.currency,
      units: transaction.units,
      total: transaction.total,
      fees: transaction.fees,
    }).then((sell) => {
      console.log("Sell of Transaction ID " + sell.transactionId + " saved to database!");
      console.log("");
      resolve(sell);
    });
  });
}

function findBuyById(id) {
  return new Promise((resolve) => {
    Buy.findOne({ transactionId: id }).then((buy) => {
      resolve(buy);
    });
  });
}

function findSellById(id) {
  return new Promise((resolve) => {
    Sell.findOne({ transactionId: id }).then((sell) => {
      resolve(sell);
    });
  });
}

function getBuyWithNoSells() {
  return new Promise((resolve) => {
    Buy.find({ sold: false }).then((buys) => {
      resolve(buys);
    });
  });
}

function updateBuy(buy) {
  return new Promise((resolve, reject) => {
    Buy.findById(buy._id, function (err, buyFromDb) {
      if (err) {
        reject();
      } else {
        buyFromDb.set(buy);
        buyFromDb.save(function (err) {
          if (err) {
            reject();
          } else {
            resolve();
          }
        });
      }
    });
  });
}

function findUnsoldBuyByCurrency(currency) {
  return new Promise((resolve) => {
    Buy.findOne({ currency: currency, sold: false }).then((sell) => {
      resolve(sell);
    });
  });
}

module.exports = {
  init,
  saveBuyToDb,
  saveSellToDb,
  findBuyById,
  findSellById,
  getBuyWithNoSells,
  updateBuy,
  findUnsoldBuyByCurrency,
};
